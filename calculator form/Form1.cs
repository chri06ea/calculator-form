﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            equationBox.Text += "1";
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            equationBox.Text += "2";
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            equationBox.Text += "3";
        }
        private void Button4_Click(object sender, EventArgs e)
        {
            equationBox.Text += "4";
        }
        private void Button5_Click(object sender, EventArgs e)
        {
            equationBox.Text += "5";
        }
        private void Button6_Click(object sender, EventArgs e)
        {
            equationBox.Text += "6";
        }
        private void Button7_Click(object sender, EventArgs e)
        {
            equationBox.Text += "7";
        }
        private void Button8_Click(object sender, EventArgs e)
        {
            equationBox.Text += "8";
        }
        private void Button9_Click(object sender, EventArgs e)
        {
            equationBox.Text += "9";
        }
        private void Button0_Click(object sender, EventArgs e)
        {
            equationBox.Text += "0";
        }
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            equationBox.Text += "+";
        }
        private void ButtonSubtract_Click(object sender, EventArgs e)
        {
            equationBox.Text += "-";
        }
        private void ButtonMultiply_Click(object sender, EventArgs e)
        {
            equationBox.Text += "*";
        }
        private void ButtonDiv_Click(object sender, EventArgs e)
        {
            equationBox.Text += "/";
        }
        private void ButtonResult_Click(object sender, EventArgs e)
        {
            Calculate();
        }
        private void Button12_Click(object sender, EventArgs e)
        {
            this.equationBox.Text = "";
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Return:
                    Calculate();
                    break;
                case Keys.Back:
                    if (equationBox.Text.Length > 0)
                        equationBox.Text = equationBox.Text.Remove(equationBox.Text.Length - 1);
                    break;
                case Keys.D0:
                    equationBox.Text += "0";
                    break;
                case Keys.D1:
                    equationBox.Text += "1";
                    break;
                case Keys.D2:
                    equationBox.Text += "2";
                    break;
                case Keys.D3:
                    equationBox.Text += "3";
                    break;
                case Keys.D4:
                    equationBox.Text += "5";
                    break;
                case Keys.D5:
                    equationBox.Text += "5";
                    break;
                case Keys.D6:
                    equationBox.Text += "6";
                    break;
                case Keys.D7:
                    equationBox.Text += "7";
                    break;
                case Keys.D8:
                    equationBox.Text += "8";
                    break;
                case Keys.D9:
                    equationBox.Text += "9";
                    break;
                case Keys.Add:
                case Keys.Oemplus:
                    equationBox.Text += "+";
                    break;
                case Keys.Subtract:
                case Keys.OemMinus:
                    equationBox.Text += "-";
                    break;
                case Keys.Divide:
                    equationBox.Text += "/";
                    break;
                case Keys.Multiply:
                    equationBox.Text += "*";
                    break;
                default:
                    return base.ProcessCmdKey(ref msg, keyData);

            }
            return true;
        }
        private void Calculate()
        {
            var equationText = equationBox.Text;
            var numbers = new List<string>(equationText.Split('+', '-', '/', '*'));
            var operators = new List<char>(Regex.Replace(equationText, "[0-9]", "").ToCharArray());
            var result = 0.0;
            var first = true;

            try
            {
                while (numbers.Count > 0)
                {
                    //user used '-' infront of a number to describe a negative number.
                    if (numbers[0].Length == 0 && operators[0] == '-')
                    {
                        numbers.RemoveAt(0);
                        numbers[0] = "-" + numbers[0];
                        operators.RemoveAt(0);
                    }

                    var number = double.Parse(numbers[0]);
                    numbers.RemoveAt(0);

                    if (first)
                    {
                        first = false;
                        result = number;
                        continue;
                    }

                    var op = operators[0];
                    operators.RemoveAt(0);

                    switch (op)
                    {
                        case '+':
                                result += number;
                                break;
                        case '-':
                                result -= number;
                                break;
                        case '*':
                                result *= number;
                                break;
                        case '/':
                                result /= number;
                                break;
                    }
                }
                equationBox.Text = equationText + "=" + result.ToString();
            }
            catch (Exception exception)
            {
                equationBox.Text = exception.Message;
            }
        }
    }
}
